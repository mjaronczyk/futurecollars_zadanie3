history = []
assortment = {}
balance = 1000000

history.append((f'Saldo, {1000000}, initial amount'))

while True:
    print()
    print('Choose one of the following options: \nsaldo / kupno / sprzedaz / wyjscie ')
    print()
    action = input("\nPress the action type: ")

    if action == 'saldo':
        
        price = int(input("Amount: "))
        comment = input("Comment: ")
        history.append((f'Saldo, {price}, {comment}'))
        balance += price
        print(f'\nDodano:\nSaldo, {price}, {comment}')


    elif action == 'kupno':
        
        id = input("Product ID: ")
        price = int(input("Price: "))
        quantity = int(input("Quantity: "))
        if (price * quantity < balance) and (quantity > 0 and price > 0):
            balance -= price * quantity
            history.append((f'Bought, id: {id}, price: {price}, qty: {quantity}'))
            print(f'Bought, id: {id}, price: {price}, qty: {quantity}')
            try:
                assortment[id] += quantity
            except:
                assortment[id] = quantity
        else:
            print("Error occured. Try again!")

    elif action == 'sprzedaz':
       
        id = input("Product ID: ")
        price = int(input("Price: "))
        quantity = int(input("Quantity: "))
        try:
            if (price > 0 and quantity > 0) and (assortment.get(id) >= quantity):
                balance += price * quantity
                assortment[id] -= quantity
                history.append((f'Sold, id: {id}, price: {price}, qty: {quantity}'))
                print(f'Sold, id: {id}, price: {price}, qty: {quantity}')
            else:
                print("Error occured. Make sure qty and price are correct")
        except:
            print("Make sure the product is in stock ")

    elif action == "wyjscie":
        break
    
    else:
        print("Incorect Type. Try again")
        


print(f'\nBalance: {balance}\n')

print('Assortment:')
for i, k in assortment.items():
    print(f'Id: {i}: Qty: {k}')

print(f'\nActions: {len(history)}')
for index, i in enumerate(history):
    print(f'{index + 1}: {i}')
        